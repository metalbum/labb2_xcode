//
//  Backend.m
//  OnceUponATime
//
//  Created by IT-Högskolan on 2015-02-01.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "Backend.h"

@interface Backend()
@property NSArray *adjectives;
@property NSArray *nouns;
@property NSArray *profNouns;
@property NSArray *verbs;
@property NSArray *pastTenseVerbs;
@property float sliderValue;

@end

@implementation Backend

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.adjectives = @[@"elak", @"fet", @"stor", @"suverän", @"tjock"];
        self.profNouns = @[@"idiot", @"hundjävel", @"dumskalle"];
        self.nouns = @[@"dumsnut", @"hundvalp", @"kille"];
        self.verbs = @[@"döda", @"spöa upp", @"injicera", @"sno", @"sparka", @"springa", @"skjuta", @"slå"];
        self.pastTenseVerbs = @[@"injicerade", @"sköt", @"skar", @"dödade", @"bet"];
    }
    return self;
}

- (NSString*) randomElement: (NSArray*) array {
    return array[arc4random_uniform(array.count)];
}

-(NSString*) blaha:(NSArray*) noun {
    
    if (_shortTale) {
        return [NSString stringWithFormat:@"Det var en gång en %@ %@ som %@ en %@ som han hittat. Han skrek: '%@ inte en %@!' och sedan %@ han en %@ med en %@ %@", [self randomElement:self.adjectives], [self randomElement:noun], [self randomElement:self.pastTenseVerbs], [self randomElement:noun], [self randomElement:self.verbs], [self randomElement:noun], [self randomElement:self.pastTenseVerbs], [self randomElement:noun], [self randomElement:self.adjectives], [self randomElement:noun]];
    } else {
        return [NSString stringWithFormat:@"Det var en gång en %@ %@ som %@ en %@ som han hittat. Han skrek: '%@ inte en %@!' och sedan %@ han en %@ med en %@ %@. Sedan kom det en %@ %@ som %@ en %@.  Plötsligt hördes: '%@ inte en %@!' och sedan %@ han en %@ som en %@ %@", [self randomElement:self.adjectives], [self randomElement:noun], [self randomElement:self.pastTenseVerbs], [self randomElement:noun], [self randomElement:self.verbs], [self randomElement:noun], [self randomElement:self.pastTenseVerbs], [self randomElement:noun], [self randomElement:self.adjectives], [self randomElement:noun], [self randomElement:self.adjectives], [self randomElement:noun], [self randomElement:self.pastTenseVerbs], [self randomElement:noun], [self randomElement:self.verbs], [self randomElement:noun], [self randomElement:self.pastTenseVerbs], [self randomElement:noun], [self randomElement:self.adjectives], [self randomElement:noun]];
    }
}

- (NSString*) generateString:(float)sliderValue {
    
    _sliderValue = sliderValue * 100;
    
    NSLog(@"%.0f", _sliderValue);
    
    if (_profanity) {
        return [self blaha:(_profNouns)];
    } else {
        return [self blaha:(_nouns)];
    }
}

@end
