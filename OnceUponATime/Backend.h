//
//  Backend.h
//  OnceUponATime
//
//  Created by IT-Högskolan on 2015-02-01.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Backend : NSObject

@property bool profanity;
@property bool shortTale;

-(NSString*) generateString:(float)sliderValue;

@end
