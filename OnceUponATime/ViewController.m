//
//  ViewController.m
//  OnceUponATime
//
//  Created by IT-Högskolan on 2015-01-26.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "ViewController.h"
#import "Backend.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *taleText;
@property (weak, nonatomic) IBOutlet UISwitch *profanitySwitch;
@property (weak, nonatomic) IBOutlet UISwitch *shortTaleSwitch;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property float sliderValue;
@property (nonatomic) Backend *backend;
@end

@implementation ViewController

- (IBAction)generateButton:(id)sender {
    [self generate];
}
- (IBAction)theSwitch:(id)sender {
    [self isProfanityOn];
}

- (IBAction)theShortTaleSwitch:(id)sender {
    [self isShortTaleOn];
}
- (IBAction)theSlider:(id)sender {
    _sliderValue = self.slider.value;
}

-(void)isShortTaleOn {
    _backend.shortTale = self.shortTaleSwitch.isOn;
}


- (void)isProfanityOn {
    _backend.profanity = self.profanitySwitch.isOn;
}

- (void)generate {
    self.taleText.text = [_backend generateString:(_sliderValue)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _backend = [[Backend alloc] init];
    [self isProfanityOn];
    [self isShortTaleOn];
    [self generate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
